""" helper functions for unit tests """

# pylint: disable=no-absolute-import
import itertools

def get_order(search_order, matching_orders):
  """ Check if `search_order` is in the order book; used for unit tests
  Ignores the value of 'action' attribute
  """
  if not matching_orders:
    return False
  for order in matching_orders[0][search_order['price']]:
    if ((order['order_id'] == search_order['order_id']) and
            (order['side'] == search_order['side']) and
            (order['quantity'] == search_order['quantity'])):
      return order
  return False

def get_ob_len(buy_orders, sell_orders):
  """ manually iterate buy & sell order maps to compute the length to see it
  matches with true length, only used for unit tests """
  count = 0
  for product in itertools.chain(sell_orders.values(),
                                  buy_orders.values()):
    for orders in product.values():
      count = count + len(orders)
  return count
