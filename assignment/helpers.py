""" Helper functions """

# pylint: disable=no-absolute-import
import re

def validate_message(msg: str):
  """ Check if the incoming message is in valid format """
  pattern = r'^(N,\d{1,5}|R|M),\d{1,10},[S|B],\d{1,3},\d{1,6}$|^X,\d{1,5},\d{1,3},\d{1,6}$'
  return re.search(pattern, msg) is not None

def parse(msg: str):
  """ Parse messages from string format to Python dict format
  This is a private function that assumes `str` has a valid message format and
  is supposed to be called after `str` has been validated using `validate_message`
  """
  parsed = list(map(lambda v: v.strip(), msg.strip().split(',')))
  parsed[-1], parsed[-2] = int(parsed[-1]), int(parsed[-2])
  if parsed[-1] == 0 or parsed[-2] == 0:
    return False

  # can use parsed[0] == 'N'
  if msg.startswith('N'):
    return dict(zip(['action', 'product_id', 'order_id', 'side', 'quantity', 'price'], parsed))
  if msg.startswith('R') or msg.startswith('M'):
    return dict(zip(['action', 'order_id', 'side', 'quantity', 'price'], parsed))
  # if msg.startswith('X')
  return dict(zip(['action', 'product_id', 'quantity', 'price'], parsed))

def to_dict(msg: str, errors):
  """ Validate and parse messages from string format to Python dict format """
  if not validate_message(msg):
    # can pass `msg` to a function to collect more specific error messages by
    # detecting negative values & missing values using regular expressions
    errors.append(f'Corrupted message format: {msg}')
    return False
  return  parse(msg)

def process_messages(messages, book):
  """ process a single message and print products for every 10th message """
  count = 0
  for msg in messages:
    success = book.process_message(msg)
    if not success:
      continue

    if count == 9:
      print(f'\n{count + 1} more messages processed, products as follows:')
      print(book.print_products())
      print('\n====================================================\n')
    count = (count + 1) % 10
  return book

def cleanup_after_remove(m_ords, order):
  """ clean up order book after removing an order """
  tree = m_ords[0]
  all_orders = m_ords[1]
  product_id = m_ords[2]

  # if removing the order with order.order_id makes the tree for this price
  # empty, remove this price tree also
  if order['price'] in tree and (len(tree[order['price']]) == 0):
    tree.pop(order['price'])
  # if removing the above  tree makes the hash table slot for this product
  # empty, remove the slot also
  if product_id in all_orders and (len(all_orders[product_id]) == 0):
    all_orders.pop(product_id)
