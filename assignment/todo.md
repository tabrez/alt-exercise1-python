# TODO

* add support for sell orders
* add support for modifying orders
* collect errors and print them before exiting program
* print products after every 10th message
* read messages from a file
* print how much quantity traded at most recent trade price for each product
* Check on each message if there's a trade message when it's expected
* Check on each trade message if it's unexpected

* change name `prices` to `dicts` and `convert_to_dicts` to `parse`
* replace loop in `remove_order` and `modify_order` with a generator or higher
  order function
* maybe use decorators to validate & parse text messages when calling
  insert/modify/remove order methods
* write tests to make sure insert, delete, modify happened under correct product
  id & price
* convert multiple if branches to function calls using dict of values used in
  conditions to function object mapping
* return Optional/Maybe where appropriate(especially after `validate_msg` is
  integrated in `covert_to_dict`)
* use underscores before names of private variables & methods
* split functions into multiple modules
* explain parameters & add examples/doctest tests in docstrings
* add types and check via mypy
* don't store product id, price in book order records
* can modify action increase the quantity?
