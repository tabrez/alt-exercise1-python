""" An order book manager """

# pylint: disable=no-absolute-import
import collections
import itertools
import sys
import sortedcontainers

import helpers

class OrderBook:
  """ Collections to store buy orders and sell orders """
  def __init__(self):
    self.buy_orders = collections.defaultdict(sortedcontainers.SortedDict)
    self.sell_orders = collections.defaultdict(sortedcontainers.SortedDict)
    # mapping between order ids and product ids
    self.po_map = {}
    self.recent_trades = {}
    self.trade_msg_expected = False
    self.errors = []

  def insert_order(self, order):
    """ Insert a new record into the order book when 'N,x,x,x,x,x' message is received
    """
    if order['order_id'] in self.po_map:
      return False

    orders = self.buy_orders if order['side'] == 'B' else self.sell_orders
    products = orders[order['product_id']]
    # [] could be sortedcontainers.SortedList if we want to store all the orders
    # sorted by their timestamp/sequence number
    # order[side], order[product_id], order[price] don't need to be stored after
    # sufficient unit testing is done
    products.setdefault(order['price'], []).append(order)

    self.po_map[order['order_id']] = order['product_id']

    return True

  def get_matching_orders(self, order):
    """ Retrieve all the orders from the order book that match the given order price """
    if not order['order_id'] in self.po_map:
      return False

    product_id = self.po_map[order['order_id']]
    all_orders = self.buy_orders if order['side'] == 'B' else self.sell_orders
    products = all_orders[product_id]

    if not order['price'] in products:
      return False
    return products, all_orders, product_id

  def modify_order(self, mod_order):
    """ Modify a record from the order book when "M,x,x,x,x' message is received
    """
    orders = self.get_matching_orders(mod_order)
    if not orders:
      return False

    for order in orders[0][mod_order['price']]:
      if order['order_id'] == mod_order['order_id']:
        order['quantity'] = mod_order['quantity']
        return True
    return False

  def remove_order(self, rem_order):
    """ Remove a record from the order book when "R,x,x,x,x' message is received
    """
    m_ords = self.get_matching_orders(rem_order)

    if not m_ords:
      return False

    for order in m_ords[0][rem_order['price']]:
      if order['order_id'] == rem_order['order_id']:
        if order['quantity'] == rem_order['quantity']:
          m_ords[0][rem_order['price']].remove(order)
          del self.po_map[order['order_id']]
          helpers.cleanup_after_remove(m_ords, rem_order)
          return True
        return False
    return False

  def is_trade_message_expected(self, order):
    """ When a new message is received, check if trade is possible
    """
    if order['action'] != 'N' and order['action'] != 'M':
      return False
    if order['action'] == 'N' and order['order_id'] in self.po_map:
      return False

    orders = self.buy_orders if order['side'] == 'S' else self.sell_orders

    # fill in product_id if it's M,x,x,x,x message
    if order['action'] == 'M':
      if order['order_id'] not in self.po_map:
        return False
      order['product_id'] = self.po_map[order['order_id']]

    products = orders[order['product_id']]

    if order['side'] == 'S':
      prices = products.irange(order['price'], sys.maxsize)
    else:
      prices = products.irange(0, order['price'])

    # maybe manually iterate over orders if these lists can become large
    qualifying_orders = [products[price] for price in prices]
    flattened = list(itertools.chain(*qualifying_orders))

    total_quantity = 0
    for value in flattened:
      total_quantity = total_quantity + value['quantity']
      if total_quantity >= order['quantity']:
        return True
    return False

  def is_it_expected_msg(self, msg, order):
    """ check if the given message is expected when it's received

    `status` variable is helpful for unit testing:
    `trade-expected` = True means a trade message was expected but not received
    `trade-not-expected` = True means a trade was not expected but was received
    """
    status = {'trade-expected': False, 'trade-not-expected': False}
    if order['action'] == 'X':
      if not self.trade_msg_expected:
        status['trade-not-expected'] = True
        self.errors.append(f'Recieved trade action when such trade is not possible; '\
                           f'received message: {msg}')
    else:
      if self.trade_msg_expected:
        status['trade-expected'] = True
        self.errors.append(f'Trade is expected but no trade action received; '\
                           f'received message: {msg}')
    return status

  def update_recent_trades(self, order):
    """ get recent trades at given product id & price """
    if order['product_id'] not in self.recent_trades:
      self.recent_trades[order['product_id']] = {
        'quantity': order['quantity'],
        'price': order['price']
        }
      return
    product = self.recent_trades[order['product_id']]
    if product['price'] == order['price']:
      product['quantity'] = product['quantity'] + order['quantity']
    else:
      product['quantity'] = order['quantity']
      product['price'] = order['price']

  def process_message(self, msg):
    """ process a single message and update the order book accordingly """
    order = helpers.to_dict(msg, self.errors)
    if not order:
      return False

    def handle_new_message(order):
      """ handle new messages of the form N,x,x,x,x,x """
      self.trade_msg_expected = self.is_trade_message_expected(order)


      res = self.insert_order(order)
      if not res:
        self.errors.append(f'Order with id {order["order_id"]} already exists; '
                           f'received msg: {msg}')
      return res

    def handle_modify_message(order):
      """ handle modify messages of the form M,x,x,x,x """
      res = self.modify_order(order)
      if not res:
        self.errors.append(f'Order with specified details doesn\'t exist; '
                          f'received message: {msg}')
      return res

    def handle_remove_message(order):
      """ handle remove messages of the form R,x,x,x,x """
      res = self.remove_order(order)
      if not res:
        self.errors.append(f'Order with specified details doesn\'t exist; '
                          f'received message: {msg}')
      return res

    def handle_trade_message(order):
      """ handle trade messages of the form X,x,x,x """

      self.update_recent_trades(order)
      trade = self.recent_trades[order['product_id']]
      print(f'{msg.strip()} => product {order["product_id"]}: ' \
                  f'{trade["quantity"]} @ {trade["price"]}')
      return True

    self.is_it_expected_msg(msg, order)
    self.trade_msg_expected = self.is_trade_message_expected(order)
    handlers = {
      'N': handle_new_message,
      'M': handle_modify_message,
      'R': handle_remove_message,
      'X': handle_trade_message
    }

    res = handlers[order['action']](order)
    return res

  def print_products(self):
    """ print some entries of each product in the order book """
    output = ''
    for pid, product in itertools.chain(self.buy_orders.items(),
                                          self.sell_orders.items()):
      output = output + f'\nProduct ID: {pid} =>'
      for price in product.islice(0,4):
        output = output + f'\n--Price: {price}'
        for order in product[price][0:4]:
          for key in ['order_id', 'side', 'quantity']:
            output = output + f'\n    {key}: {str(order[key])}; '
          output = output + '\n'
    return output

  def __repr__(self):
    return f'\n\nBuy orders: \n{self.buy_orders.__repr__()} \
              \n\nSell orders: \n{self.sell_orders.__repr__()} \
              \n\nProducts/Orders map: \n{self.po_map.__repr__()} '

  def __len__(self):
    return len(self.po_map)
