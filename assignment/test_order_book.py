""" Test Order Book functionality """

# pylint: disable=no-absolute-import
import order_book as ob
import helpers_test as ht
import helpers

def test_init():
  """ Test construction of OrderBook instances """
  book = ob.OrderBook()
  assert not book.buy_orders
  assert not book.sell_orders
  assert not book.po_map
  assert not book.trade_msg_expected
  assert not book.recent_trades
  assert not book.errors

def test_validate_msg_correct_msgs():
  """ Test `validate_msg` function for messages in correct format """
  correct_msgs = [
    'N,5,100001,B,1,175',
    'R,100001,B,1,175',
    'M,100001,B,1,175',
    'N,5,100001,S,1,175',
    'R,100001,S,1,175',
    'M,100001,S,1,175',
    'X,5,1,1050',
  ]
  for msg in correct_msgs:
    assert helpers.validate_message(msg)

def test_validate_msg_corrupted_msgs():
  """ Test `validate_msg` function for messages in incorrect format """
  corrupted_msgs = [
    'hello world',
    '12345',
    'N,5,100001,T,1,175',
    'R,5,100001,B,1,175',
    'M,5,100001,B,1,175',
    'X,5,100001,B,1,175',
    'R,B,1,175',
    'M,100001,1,175',
    'N,5,100001,S,175',
    'N,5,100001,B,0,175',
    'N,5,100001,B,5,0',
    'R,100001,S,1',
    'M,100001,S,hello,175',
    'M,100001,Y,hello,175',
    'X,1,1050',
    'N,1,1050',
    'R,1,1050',
    'M,1,1050',
    'Y,1,2,1050',
    'Y,5,100001,B,1,175',
    'Y,100001,B,1,175',
  ]
  for msg in corrupted_msgs:
    assert not helpers.validate_message(msg) or not helpers.parse(msg)

def test_validate_msg_neg_values_in_msgs():
  """ Test `validate_msg` function for messages containing negative numbers """
  negative_number_msgs = [
    'N,-5,100001,B,1,175',
    'R,-100001,S,1,175',
    'M,100001,B,-1,175',
    'N,5,100001,S,1,-175',
    'N,-5,-100001,B,-1,-175',
   ]
  for msg in negative_number_msgs:
    assert not helpers.validate_message(msg)

def test_validate_msg_missing_values_in_msgs():
  """ Test `validate_msg` function for messages containing missing values """
  missing_number_msgs = [
    'N,100001,B,1,175',
    'N,100001,S,1,175',
    'N,10,1,175',
    'N,10,1,175',
    'N,100001,B,175',
    'N,100001,S,175',
    'N,100001,S',
    'N,S,175',
    'R,B,1,175',
    'R,100001,1,175',
    'R,100001,B,1',
    'R,100001,B,',
    'R,S,1'
    'M,B,1,175',
    'M,100001,1,175',
    'M,100001,S,1',
    'M,100001,S,',
    'M,S,1'
  ]
  for msg in missing_number_msgs:
    assert not helpers.validate_message(msg)

def test_insert_order_correct_msgs():
  """ Test `insert_order` for messages in correct format """
  correct_messages = [
  'N,5,100001,B,1,175',
  'N,5,100002,B,11,175',
  'N,5,100003,S,4,1075',
  'N,4,100004,S,13,1107',
  'N,4,100005,B,13,1107',
  'N,3,100006,B,2,175'
  ]
  book = ob.OrderBook()
  for msg in correct_messages:
    order = helpers.to_dict(msg, book.errors)
    assert book.insert_order(order)
    assert ht.get_order(order, book.get_matching_orders(order))
  assert len(book.buy_orders) + len(book.sell_orders) == 5

def test_insert_order_duplicate_orderids():
  """ Test `insert_order` for messages that have duplicate order ids """
  book = ob.OrderBook()
  assert book.insert_order(helpers.to_dict('N,5,100001,B,1,175', book.errors))
  assert not book.insert_order(helpers.to_dict('N,5,100001,B,1,175', book.errors))
  assert not book.insert_order(helpers.to_dict('N,5,100001,S,1,175', book.errors))
  assert not book.insert_order(helpers.to_dict('N,4,100001,B,1,1750', book.errors))

  assert book.insert_order(helpers.to_dict('N,4,100002,S,1,175', book.errors))
  assert not book.insert_order(helpers.to_dict('N,4,100002,S,1,175', book.errors))

def test_remove_order_correct_msgs():
  """ Test `remove_order` for messages in correct format """
  correct_messages = [
  'N,5,100001,B,3,1050',
  'R,100001,B,3,1050',
  'N,5,100002,B,4,2000',
  'N,5,100003,S,4,2000',
  'R,100002,B,4,2000',
  'R,100003,S,4,2000'
  ]
  book = ob.OrderBook()
  for msg in correct_messages:
    new_order = helpers.to_dict(msg, book.errors)
    if new_order['action'] == 'N':
      book.insert_order(new_order)
    elif new_order['action'] == 'R':
      assert book.remove_order(new_order)
      assert not ht.get_order(new_order, book.get_matching_orders(new_order))

def test_remove_order_incorrect_msgs():
  """ Test `remove_order` for messages in incorrect format specific to removing
  orders """
  incorrect_msgs = [
  'N,5,100001,B,3,1050',
  'R,100001,B,3,105',
  'N,6,100006,S,6,1250',
  'R,100006,S,5,1250',
  'R,100006,B,6,1250',
  'R,100007,B,6,1250',
  'R,100006,S,6,1250',
  'R,100006,S,6,1250'
  ]
  book = ob.OrderBook()
  book.insert_order(helpers.to_dict(incorrect_msgs[0], book.errors))
  assert not book.remove_order(helpers.to_dict(incorrect_msgs[1], book.errors))
  book.insert_order(helpers.to_dict(incorrect_msgs[2], book.errors))
  assert not book.remove_order(helpers.to_dict(incorrect_msgs[3], book.errors))
  assert not book.remove_order(helpers.to_dict(incorrect_msgs[4], book.errors))
  assert not book.remove_order(helpers.to_dict(incorrect_msgs[5], book.errors))
  book.remove_order(helpers.to_dict(incorrect_msgs[6], book.errors))
  assert not book.remove_order(helpers.to_dict(incorrect_msgs[7], book.errors))

def test_remove_order_orderid_missing():
  """ Test `remove_order` for messages that have duplicate order ids """
  book = ob.OrderBook()
  assert not book.remove_order(helpers.to_dict('R,100001,B,1,175', book.errors))
  assert not book.remove_order(helpers.to_dict('R,100001,S,1,175', book.errors))

  book.insert_order(helpers.to_dict('N,5,100001,B,1,175', book.errors))
  assert book.remove_order(helpers.to_dict('R,100001,B,1,175', book.errors))
  assert not book.remove_order(helpers.to_dict('R,100001,B,1,175', book.errors))

  book.insert_order(helpers.to_dict('N,5,100001,S,1,175', book.errors))
  assert book.remove_order(helpers.to_dict('R,100001,S,1,175', book.errors))
  assert not book.remove_order(helpers.to_dict('R,100001,S,1,175', book.errors))

def test_modify_order_correct_msgs():
  """ Test `modify_order` for messages in correct format """
  correct_messages = [
  'N,3,100001,B,3,1050',
  'M,100001,B,5,1050',
  'N,4,100002,B,4,2000',
  'N,15,100003,S,4,2000',
  'M,100002,B,3,2000',
  'M,100003,S,1,2000'
  ]
  book = ob.OrderBook()
  for msg in correct_messages:
    new_order = helpers.to_dict(msg, book.errors)
    if new_order['action'] == 'N':
      assert book.insert_order(new_order)
    elif new_order['action'] == 'M':
      assert book.modify_order(new_order)

def test_modify_order_incorrect_msgs():
  """ Test `modify_order` for messages in incorrect format specific to modifying
  orders """
  book = ob.OrderBook()
  book.insert_order(helpers.to_dict('N,5,100001,B,3,1050', book.errors))
  assert not book.modify_order(helpers.to_dict('M,100002,B,4,1050', book.errors))
  assert not book.modify_order(helpers.to_dict('M,100001,S,5,1050', book.errors))
  assert not book.modify_order(helpers.to_dict('M,100002,B,2,105', book.errors))

def test_modify_remove_order_interaction():
  """ Test if remove works correctly on a record whose quantity was modified """
  book = ob.OrderBook()
  book.insert_order(helpers.to_dict('N,5,100001,B,3,1050', book.errors))
  book.modify_order(helpers.to_dict('M,100001,B,11,1050', book.errors))
  assert not book.remove_order(helpers.to_dict('R,100001,B,3,1050', book.errors))
  assert book.remove_order(helpers.to_dict('R,100001,B,11,1050', book.errors))

def test_all_operations_correct_msgs():
  """ Test insert, modify, remove operations on a series of correct messages """
  messages = [
    'N,5,100001,B,1,175',
    'N,5,100002,B,11,175',
    'R,100001,B,1,175',
    'N,5,100003,B,4,1075',
    'N,4,100004,B,13,1107',
    'N,4,100005,S,13,1107',
    'R,100003,B,4,1075',
    'N,3,100006,B,2,175',
    'R,100006,B,2,175',
    'N,5,100001,S,1,175',
    'M,100002,B,17,175',
    'R,100001,S,1,175',
    'N,5,100003,S,4,1075',
    'M,100004,B,9,1107',
    'M,100005,S,4,1107',
    'R,100005,S,4,1107',
    'R,100003,S,4,1075',
    'N,3,100006,S,2,175',
    'R,100006,S,2,175'
  ]
  book = ob.OrderBook()
  for msg in messages:
    new_order = helpers.to_dict(msg, book.errors)
    if new_order['action'] == 'N':
      assert book.insert_order(new_order)
    elif new_order['action'] == 'M':
      assert book.modify_order(new_order)
    elif new_order['action'] == 'R':
      assert book.remove_order(new_order)

def test_check_is_msg_expected():
  """ Test if `is_msg_expected` works correctly """
  book = ob.OrderBook()

  # trade message not expected but got X action
  msg = 'X,5,2,1000'
  assert not book.trade_msg_expected
  errors = book.is_it_expected_msg(msg, helpers.to_dict(msg, book.errors))
  assert errors['trade-not-expected']
  assert not errors['trade-expected']

  # trade message expected and got X action
  book.trade_msg_expected = True
  errors = book.is_it_expected_msg(msg, helpers.to_dict(msg, book.errors))
  assert not errors['trade-not-expected']
  assert not errors['trade-expected']

  # trade message not expected and got N action
  msg = 'N,3,100006,S,2,175'
  book.trade_msg_expected = False
  errors = book.is_it_expected_msg(msg, helpers.to_dict(msg, book.errors))
  assert not errors['trade-not-expected']
  assert not errors['trade-expected']

  # trade message expected but got N action
  book.trade_msg_expected = True
  errors = book.is_it_expected_msg(msg, helpers.to_dict(msg, book.errors))
  assert not errors['trade-not-expected']
  assert errors['trade-expected']

def test_check_if_trade_is_expected():
  """ Test if `check_if_trade_is_expected` works correctly for a sequence of N
  actions """
  messages = [
    'N,5,100001,B,1,175',
    'N,5,100002,B,11,175',
    'N,5,100003,B,7,170',
    'N,5,100004,B,14,85',  # can't trade, no sell orders
    'N,5,100005,S,3,200',  # can't trade, no buy orders <= 200
    'N,5,100006,B,3,75'    # can't trade, no sell orders => 75
  ]
  book = ob.OrderBook()
  for msg in messages:
    assert not book.is_trade_message_expected(helpers.to_dict(msg, book.errors))
    book.process_message(msg)
    assert not book.trade_msg_expected

  # can trade, >= 12 quantity of buy orders >= 175 present
  assert book.is_trade_message_expected(helpers.to_dict('N,5,100007,S,12,175', book.errors))
  # can't trade, >=13 quantity of buy orders >= 175 not present
  assert not book.is_trade_message_expected(helpers.to_dict('N,5,100007,S,13,175', book.errors))
  # can trade, >=19 quantity of buy orders >= 170 present
  assert book.is_trade_message_expected(helpers.to_dict('N,5,100007,S,19,165', book.errors))
  # can't trade, >=20 quantity of buy orders >= 165 not present
  assert not book.is_trade_message_expected(helpers.to_dict('N,5,100007,S,20,165', book.errors))

  # can trade, >=2 quantity of sell orders >= 200 not present
  assert book.is_trade_message_expected(helpers.to_dict('M,100006,B,2,200', book.errors))
  # can't trade, >=4 quantity of buy orders >= 200 not present
  assert not book.is_trade_message_expected(helpers.to_dict('M,100006,B,4,200', book.errors))

  # can trade, >= 12 quantity of buy orders >= 175 present
  book.process_message('N,5,100007,S,12,175')
  assert book.trade_msg_expected

  # can't trade, order id doesn't exist for M message
  book.process_message('M,1000017,S,12,175')
  assert not book.trade_msg_expected

  # can't trade, order id exists for N message
  book.process_message('N,100007,S,12,175')
  assert not book.trade_msg_expected

def test_check_if_trade_is_expected_x_action():
  """ Test if `check_if_trade_is_expected` works correctly when X actions are
  present """
  messages =  [
    'N,5,100001,B,1,175',
    'N,5,100002,B,11,175',
    'N,5,100003,B,7,170',
    'N,5,100004,B,14,85',  # can't trade, no sell orders
    'N,5,100005,S,1,200',  # can't trade, no buy orders <= 200
    'N,5,100006,B,3,75',   # can't trade, no sell orders => 75
    'N,5,100007,S,5,175',  # can trade
  ]
  book = ob.OrderBook()
  for msg in messages:
    book.process_message(msg)
  assert book.trade_msg_expected

  msg = 'X,5,2,1000'
  status = book.is_it_expected_msg(msg, helpers.to_dict(msg, book.errors))
  assert not status['trade-not-expected']
  assert not status['trade-expected']
  res = book.process_message(msg)
  assert res

def test_get_recent_trade_quantity():
  """ test if `get_recent_trade_quantity` works correctly """
  book = ob.OrderBook()
  assert '5' not in book.recent_trades

  book.process_message('X,5,1,1000')
  assert '5' in book.recent_trades
  assert book.recent_trades['5'] == { 'quantity': 1, 'price': 1000 }
  book.process_message('X,5,1,1000')
  assert book.recent_trades['5'] == { 'quantity': 2, 'price': 1000 }
  book.process_message('X,5,1,1000')
  assert book.recent_trades['5'] == { 'quantity': 3, 'price': 1000 }

  assert '4' not in book.recent_trades
  book.process_message('X,4,1,1000')
  assert '4' in book.recent_trades
  assert book.recent_trades['5'] == { 'quantity': 3, 'price': 1000 }
  assert book.recent_trades['4'] == { 'quantity': 1, 'price': 1000 }
  book.process_message('X,4,7,1500')
  assert book.recent_trades['4'] == { 'quantity': 7, 'price': 1500 }

  book.process_message('X,5,3,1000')
  assert book.recent_trades['5'] == { 'quantity': 6, 'price': 1000 }

  assert '3' not in book.recent_trades
  book.process_message('X,3,11,900')
  assert '3' in book.recent_trades
  assert book.recent_trades['3'] == { 'quantity': 11, 'price': 900 }

  book.process_message('X,4,4,1500')
  assert book.recent_trades['4'] == { 'quantity': 11, 'price': 1500 }
  book.process_message('X,4,3,1300')
  assert book.recent_trades['4'] == { 'quantity': 3, 'price': 1300 }

  book.process_message('X,5,1,1500')
  assert book.recent_trades['5'] == { 'quantity': 1, 'price': 1500 }

# pylint: disable=too-many-statements
def test_example1():
  """ Test for most of the functionality using a large sequence of messages """
  book = ob.OrderBook()

  # orders don't exist
  assert not book.process_message('R,100001,B,3,175')
  assert not book.process_message('M,100001,B,3,175')

  assert book.process_message('N,5,100001,B,3,175')
  order = helpers.to_dict('N,5,100001,B,3,175', book.errors)
  assert ht.get_order(order, book.get_matching_orders(order))

  # inserting an order with duplicate order id
  assert not book.process_message('N,5,100001,B,1,175')
  order = helpers.to_dict('N,5,100001,B,1,175', book.errors)
  assert not ht.get_order(order, book.get_matching_orders(order))

  assert book.process_message('N,5,100002,B,11,175')
  order = helpers.to_dict('N,5,100002,B,11,175', book.errors)
  assert ht.get_order(order, book.get_matching_orders(order))

  # trade message not expected but received
  assert not book.trade_msg_expected
  msg = 'X,5,3,1500'
  assert book.process_message(msg)
  status = book.is_it_expected_msg(msg, helpers.to_dict(msg, book.errors))
  assert status['trade-not-expected']

  assert book.process_message('R,100001,B,3,175')
  order = helpers.to_dict('R,100001,B,3,175', book.errors)
  assert not ht.get_order(order, book.get_matching_orders(order))
  assert not order['order_id'] in book.po_map

  assert book.process_message('N,5,100003,B,11,175')
  order = helpers.to_dict('N,5,100003,B,11,175', book.errors)
  assert ht.get_order(order, book.get_matching_orders(order))
  assert book.process_message('N,5,100004,B,11,175')
  order = helpers.to_dict('N,5,100004,B,11,175', book.errors)
  assert ht.get_order(order, book.get_matching_orders(order))

  # trade message not expected, not received
  msg = 'N,5,100005,B,11,175'
  status = book.is_it_expected_msg(msg, helpers.to_dict(msg, book.errors))
  assert not status['trade-expected']
  assert not status['trade-not-expected']
  assert book.process_message(msg)
  order = helpers.to_dict('N,5,100005,B,11,175', book.errors)
  assert ht.get_order(order, book.get_matching_orders(order))

  # trade message not expected but received
  assert not book.trade_msg_expected
  msg = 'X,4,3,150'
  status = book.is_it_expected_msg(msg, helpers.to_dict(msg, book.errors))
  assert status['trade-not-expected']
  assert book.process_message(msg)

  assert not book.trade_msg_expected
  assert book.process_message('N,5,100006,S,1,160')
  order = helpers.to_dict('N,5,100006,S,1,160', book.errors)
  assert ht.get_order(order, book.get_matching_orders(order))

  # trade message expected but not received
  assert book.trade_msg_expected
  msg = 'N,5,100007,B,4,200'
  status = book.is_it_expected_msg(msg, helpers.to_dict(msg, book.errors))
  assert status['trade-expected']
  assert book.process_message(msg)
  order = helpers.to_dict('N,5,100007,B,4,200', book.errors)
  assert ht.get_order(order, book.get_matching_orders(order))

  assert not book.trade_msg_expected
  assert book.process_message('N,5,100008,S,1,160')
  order = helpers.to_dict('N,5,100006,S,1,160', book.errors)
  assert ht.get_order(order, book.get_matching_orders(order))

  # trade message expected and received
  assert book.trade_msg_expected
  msg = 'X,5,1,160'
  status = book.is_it_expected_msg(msg, helpers.to_dict(msg, book.errors))
  assert not status['trade-not-expected']
  assert not status['trade-expected']
  assert book.process_message(msg)


def test_example2():
  """ hello """

  prep_messages = [
    'R,100001,B,3,175',
    'M,100001,B,3,175',
    'N,5,100001,B,3,175',
    'N,5,100001,B,1,175',
    'N,5,100002,B,11,175',
    'X,5,3,1500',
    'R,100001,B,3,175',
    'N,5,100003,B,4,170',
    'N,4,100004,B,9,150',
    'N,4,100005,S,13,180',
    'X,4,3,150',
    'N,5,100006,S,1,160',
    'N,5,100007,B,4,200',
    'N,5,100008,S,1,160',
    'X,5,1,160',
    'M,100003,B,3,170',
    'R,100008,S,1,160',
    'N,4,100009,B,3,190',
    'X,4,3,190',
  ]

  book = helpers.process_messages(prep_messages, ob.OrderBook())

  msg = 'M,100005,S,10,180'
  assert book.process_message(msg)
  order = helpers.to_dict(msg, book.errors)
  retrieved_order = ht.get_order(order, book.get_matching_orders(order))
  assert retrieved_order
  assert retrieved_order['quantity'] == 10

  msg = 'R,100009,B,3,190'
  assert book.process_message(msg)
  order = helpers.to_dict(msg, book.errors)
  assert not ht.get_order(order, book.get_matching_orders(order))

def test_len():
  """ test if `len` method works correctly """

  messages = [
    'N,5,100001,B,1,175',
    'N,5,100002,B,11,175',
    'N,5,100003,S,4,1075',
    'N,4,100004,S,13,1107',
    'N,4,100005,B,13,1107',
    'N,3,100006,B,2,175'
  ]
  book = ob.OrderBook()
  count = 0
  for msg in messages:
    assert book.process_message(msg)
    order = helpers.to_dict(msg, book.errors)
    assert ht.get_order(order, book.get_matching_orders(order))
    count = count + 1
    assert len(book) == count
    assert ht.get_ob_len(book.buy_orders, book.sell_orders) == count

  # incorrect message doesn't change length
  assert not book.process_message('N,4,B,1,185')
  assert len(book) == count
  assert ht.get_ob_len(book.buy_orders, book.sell_orders) == count

  # duplicate id doesn't change length
  assert not book.process_message('N,4,100001,B,1,185')
  assert len(book) == count
  assert ht.get_ob_len(book.buy_orders, book.sell_orders) == count

  # modify message doesn't change length
  assert book.process_message('M,100002,B,3,175')
  assert len(book) == count
  assert ht.get_ob_len(book.buy_orders, book.sell_orders) == count

  # trade message doesn't change length
  assert book.process_message('X,4,1,185')
  assert len(book) == count
  assert ht.get_ob_len(book.buy_orders, book.sell_orders) == count

  # remove message decrements length
  assert book.process_message('R,100002,B,3,175')
  count = count - 1
  assert len(book) == count
  assert ht.get_ob_len(book.buy_orders, book.sell_orders) == count
