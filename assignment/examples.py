""" example test code """

# pylint: disable=no-absolute-import
import order_book as ob
import helpers

def print_errors(source, errors):
  """ hello """
  print(f'\n\nFollowing errors occurred after processing messages from {source}: ')
  for error in errors:
    print(f'{error}')

def example1():
  """ example run """
  filename = './messages.txt'
  with open(filename) as messages:
    book = helpers.process_messages(messages, ob.OrderBook())
  print(book.print_products())
  print_errors(filename, book.errors)

def example2():
  """ example run """
  array_messages = [
    'hello',
    'N,100001,B,1,175',
    'N,5,B,1,175',
    'N,5,100001,B,1,175',
    'N,5,100002,B,11,175',
    'X,5,3,1500',
    'N,5,100012,S,5,170',
    'N,5,100001,B,1,175',
    'R,100001,B,1,175',
    'N,5,100003,B,4,1075',
    'N,4,100004,B,13,1107',
    'N,4,100005,S,13,1107',
    'X,5,3,1500',
    'R,100003,B,4,1075',
    'N,3,100006,B,2,175',
    'R,100006,B,2,175',
    'N,5,100001,S,1,175',
    'M,100002,B,17,175',
    'R,100001,S,1,175',
    'R,100001,S,1,175',
    'N,5,100003,S,4,1075',
    'M,100004,B,9,1107',
    'M,100005,S,4,1107',
    'R,100005,S,4,1107',
    'R,100003,S,4,1075',
    'N,3,100006,S,2,175',
    'R,100006,S,2,175',
    'X,5,3,1500'
  ]

  book = helpers.process_messages(array_messages, ob.OrderBook())
  print(book.print_products())
  print_errors('array', book.errors)

if __name__ == "__main__":
  example1()
