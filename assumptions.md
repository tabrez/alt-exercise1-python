# Assumptions

From the document:  
> Order: productid, action, orderid, side, quantity, price (e.g.,
N,388,123,B,9,1000)

I'm assuming the example is in this order:  
(e.g., N,388,123,B,9,1000) -> action, productid, orderid, side, quantity, price

From the document:  
> Trade: action, quantity, price (e.g., X,388,2,1025)

I'm assuming the second number is the productid:  
Trade: action, productid, quantity, price (e.g., X,388,2,1025)

From the document:
> (2) Every 10 th message and on exit, write out a human-readable representation
of the book down to the 5 th level for each product id.

What does "down to the 5th level of each product id" mean. I'm printing a few
orders at a few price points for each product id.

Background says:
> when receiving a trade message, the feed handler must remove order quantity
> starting from the best price

the example shows these decisions are made elsewhere and messages are generated
accordingly:
R,100008,B,3,1050
R,100005,S,2,1025
M,100007,S,4,1025

---
`recent_trades` dict in `OrderBook` can be integrated with
buy_orders/sell_orders using tuple/dict/list if almost all products will have
recent trades:

```python
self.buy_orders = {
  'orders': collections.defaultdict(sortedcontainers.SortedDict)
  'recent_price': 0
  'quantity': 0
}
```

---
If a new messages comes in and makes it possible for a trade to happen but the
very next message is not an "X" message, I'm collecting an error. If a new
message comes in which doesn't signal a new trade but an "X" message is
received, I'm collecting an error.

---
What is out-of-bounds for price_id, order_id, quantity, price. I'm treating
product_id and order_id as strings containing digits as they're identifiers.
I've used an arbitrary limit for price in the C++ version, used arbitrary limit
on number of digits each can contain as examples of how we might want to handle
it.

---
I've sorted the orders for each product id by price first and then by order_id.
We can order them by price first, then by timestamp/sequence number also if
that's better for more tasks.

