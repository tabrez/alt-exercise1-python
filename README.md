# Python Version

All the python source code is in the `assignment` folder, change directory to it before running the following commands.

## Setup

I've verified running the program with Python 3.7 to 3.9 on Ubuntu Linux
operating system. It needs `sortedcontainers` package which can be installed using the following command:

### Method 1: Recreate my environment

* pip

```bash
python3 -m venv py38
source env/bin/activate
pip install -r requirements.txt
```

* conda

```bash
conda create --name py38 --file conda_requirement
conda activate py38
```

### Method 2: Use pip in your environment

```bash
pip install sortedcontainers
# optional
pip install pylint pytest
```

### Method 3: Use conda in your environment

```bash
conda install -c conda-forge sortedcontainers
# optional
# note: pylint not supported on python 3.9 yet
conda install -c anaconda pylint pytest
```

## Run the program

To run the program:

```bash
# change directory to project root
cd assignments
python example.py
```

The program will read messages stored in a file called `messages.txt` in the
current directory and process them.

If you want to run the lint tool on the source code, install `pylint` and run
the following command:

```bash
# cd assignments
pylint *.py
```

If you want to run the unit tests, install `pytest` and run the following
command:

```bash
# cd assignments
pytest
```

## File structure

* `order_book.py` has the main implementation of the order book.
* `examples.py` is the driver program and has couple of examples in it, including
one that processes messages from an array instead of from a file.
* `helpers.py` has helper/util functions.

* `test_order_book.py` have unit tests.
* `helpers_test.py` has helper/util functions for testing purposes.

* `messages.txt` has example messages. This can be replaced by your own file.
* `corrupted_messages.txt` has examples of different types of corrupted messages.
* `messages_explanation` is same as `messages.txt` but has some comment lines
to explain some messages.

* [assumptions.md](./assignments/../assumptions.md) has some of the assumptions I made.

* `requirements.txt` can be used to install package requirements using `pip`.
* `conda_requirements.txt` is used to recreate the `conda` environment I used to
  develop the program.
